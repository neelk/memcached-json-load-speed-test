package main


import (

    "os"
    "fmt"
    "log"
    "io/ioutil"
    "strings"
    "time"
    "sync"
    //"strconv"
    //"encoding/json"

    "github.com/bradfitz/gomemcache/memcache"

)


type keyJsonFname struct {
    key string
    fname string
}


func main() {

    testMemcache(os.Args[1], os.Args[2])

    os.Exit(0)

}


func testMemcache (addrPort string, jsonsPath string) {

    if !strings.HasSuffix("/", jsonsPath) {
        jsonsPath += "/"
    }

    files, err := ioutil.ReadDir(jsonsPath)
    if err != nil {
        log.Fatal(err)
    }

    jsonFiles := make(map[string]string)
    for _, f := range files {
        if strings.HasSuffix(f.Name(), ".json") {
            jsonFiles[strings.TrimSuffix(f.Name(), ".json")] = f.Name()
        }
    }

    var wg sync.WaitGroup

    worker := func (kF chan keyJsonFname) {

        mc := memcache.New(addrPort)

        if err != nil {
            log.Fatal(err)
        }

        for kv := range kF  {

            key := kv.key
            val, err := ioutil.ReadFile(jsonsPath + kv.fname)
            if err != nil {
                log.Fatal(err)
            }

            err = mc.Set(&memcache.Item{Key: key, Value: val})
            if err != nil {
                log.Fatal(err)
            }

        }

    }

    startWorker := func (kF chan keyJsonFname) {

        wg.Add(1)
        go worker(kF)
        wg.Done()

    }

    nOps := len(jsonFiles)
    defer timeOperation(time.Now(), nOps)
    fileChannel := make(chan keyJsonFname)
    defer close(fileChannel)

    startWorker(fileChannel)
    startWorker(fileChannel)
    startWorker(fileChannel)
    startWorker(fileChannel)

    for k, v := range jsonFiles {
        fileChannel <- keyJsonFname{key: k, fname: v}
    }

    wg.Wait()

}


func timeOperation(start time.Time, nOps int) {

    elapsed := time.Since(start)
    fmt.Printf("%d ops took %s\n", nOps, elapsed)

}
